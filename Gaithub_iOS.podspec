#
#  Be sure to run `pod spec lint Gaithub_iOS.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "GaitHub_IOS"
  spec.version      = "2.1.0"
  spec.summary      = "A short description of Gaithub_iOS."

  spec.description  = <<-DESC
                   DESC

  spec.homepage     = "http://www.gaitup.com"

  spec.license      = "MIT"

  spec.author             = { "mathieu" => "mathieu.knecht@gaitup.com" }

  spec.platform     = :ios

  spec.ios.vendored_frameworks = 'GaitHub_IOS.framework'

  spec.source       = { :git => "https://bitbucket.org/gaithub-dist/gaithub_ios_dist/raw/master/GaitHub_IOS.framework.zip", :tag => "2.1.0" }

end
