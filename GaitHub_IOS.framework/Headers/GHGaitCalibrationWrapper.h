//
//  GHGaitCalibrationWrapper.h
//  GaitHub_IOS
//
//  Created by Stanislas Heili on 26.03.19.
//  Copyright © 2019 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef GHGaitCalibrationWrapper_h
#define GHGaitCalibrationWrapper_h

@interface GHGaitCalibrationWrapper : NSObject
- (id) init;
- (id) initWithCadenceCalibrationTime: (int)cadCalibTime;
- (id) feedGyrx:(float)gyrx gyry:(float)gyry gyrz:(float)gyrz index:(int)index;
- (int)samplingFrequency;

@end
#endif /* GHGaitCalibrationWrapper_h */
