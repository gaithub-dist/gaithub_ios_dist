//
//  GHRunWristWrapper.h
//  GaitHub_IOS
//
//  Created by Stanislas Heili on 28.06.19.
//  Copyright © 2019 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GHRunWristWrapper : NSObject
- (id) init:(float)sizeM ageYear:(int)ageYear ;
- (id) feedAccx:(float)accx accy:(float)accy accz:(float)accz gyrx:(float)gyrx gyry:(float)gyry gyrz:(float)gyrz index:(int)index;
- (int) samplingFrequency;
@end

NS_ASSUME_NONNULL_END
