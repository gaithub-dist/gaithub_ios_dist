//
//  BarcodeAlgoWrapper.h
//  motionDriveFramework
//
//  Created by Karim Kanoun on 19.07.18.
//  Copyright © 2018 gaitUp. All rights reserved.
//

#ifndef BarcodeAlgoWrapper_h
#define BarcodeAlgoWrapper_h


/// This class should never be used directly
@interface GHBarcodeWrapper : NSObject
@end

@interface GHBarcodeWrapper ()
- (id) initWithTime_in_seconds:(int32_t)time_in_seconds year:(int32_t)year month:(int32_t)month day:(int32_t)day;
- (void) dealloc;
- (int) feedAccx:(float)accx accy:(float)accy accz:(float)accz ts:(uint64_t)ts;
- (id) run; //We are returning a type id because we can not have swift classes in the headers (the modulemap will not work)
- (float) getBarcodeResolution;
- (int)samplingFrequency;

@end

#endif /* BarcodeAlgoWrapper_h */
