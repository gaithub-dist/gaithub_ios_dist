//
//  GHLibWrapper.h
//  motionDriveFramework
//
//  Created by Karim Kanoun on 19.07.18.
//  Copyright © 2018 gaitUp. All rights reserved.
//

#ifndef GHLibWrapper_h
#define GHLibWrapper_h

/// This class should never be used directly
@interface GaitHubWrapper : NSObject
@end

@interface GaitHubWrapper ()
- (id) initWithMemorySize:(float_t)memorySize;
- (id) coreVersion;
- (void) dealloc;

@end

#endif /* GHLibWrapper_h */
