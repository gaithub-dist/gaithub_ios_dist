//
//  GaitHub_IOS.h
//  GaitHub_IOS
//
//  Created by Physilog999 on 26.07.18.
//  Copyright © 2018 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for GaitHub_IOS.
FOUNDATION_EXPORT double GaitHub_IOSVersionNumber;

//! Project version string for GaitHub_IOS.
FOUNDATION_EXPORT const unsigned char GaitHub_IOSVersionString[];



#import "GHBarcodeWrapper.h"
#import "GaitHubWrapper.h"
#import "GHHandsUpWrapper.h"
#import "GHGaitCalibrationWrapper.h"
#import "GHJumpWrapper.h"
#import "GHSitToStandWrapper.h"
#import "GHRunHeadWrapper.h"
#import "GHRunBeltWrapper.h"
#import "GHQuaternionWrapper.h"
#import "GHBalanceWrapper.h"
#import "GHSprintWrapper.h"
#import "GHGaitAnalysisWrapper.h"
#import "GHRunWristWrapper.h"
#import "GHRunFootWrapper.h"
#import "GHTugWrapper.h"

