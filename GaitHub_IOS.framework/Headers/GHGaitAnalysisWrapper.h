//
//  GHGaitAnalysisWrapper.h
//  GaitHub_IOS
//
//  Created by Stanislas Heili on 26.03.19.
//  Copyright © 2019 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>
#ifndef GHGaitAnalysisWrapper_h
#define GHGaitAnalysisWrapper_h

@interface GHGaitAnalysisWrapper : NSObject
- (id) initWithVal0:(float)val0 val1:(float)val1 val2:(float)val2 val3:(float)val3 val4:(float)val4 val5:(float)val5 val6:(float)val6 val7:(float)val7 val8:(float)val8 shouldFlip:(bool)shouldFlip cadence:(int)cadence;
- (id) feedAccx:(float)accx accy:(float)accy accz:(float)accz gyrx:(float)gyrx gyry:(float)gyry gyrz:(float)gyrz index:(int)index;
- (int)samplingFrequency;

@end
#endif /* GHGaitAnalysisWrapper_h */
