//
//  HandsUpAlgoWrapper.h
//  GaitHub_IOS
//
//  Created by Stanislas Heili on 19.09.18.
//  Copyright © 2018 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>

/// This class should never be used directly
@interface GHHandsUpWrapper : NSObject
- (id) init;
- (void) startMeasuringFirstShoulder:(int32_t)movementDuration;
- (bool) startMeasuringSecondShoulder;
- (int)  feedAccx:(float)accx accy:(float)accy accz:(float)accz ts:(uint64_t)ts;
- (int)  feedGyrx:(float)gyrx gyry:(float)gyry gyrz:(float)gyrz ts:(uint64_t)ts;
- (id) run;
- (int)samplingFrequency;
@end
