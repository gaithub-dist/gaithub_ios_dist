//
//  GHSprintWrapper.h
//  GaitHub_IOS
//
//  Created by Stanislas Heili on 28.06.19.
//  Copyright © 2019 Gaitup. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GHSprintWrapper : NSObject
- (id) init;
- (id) init:(float)sprint_distance;
- (id) feedAccx:(float)accx accy:(float)accy accz:(float)accz gyrx:(float)gyrx gyry:(float)gyry gyrz:(float)gyrz index:(int)index  speed_ms:(float)speed_ms;
- (int)samplingFrequency;

@end

NS_ASSUME_NONNULL_END
